package com.epam.javaweb.builder;

import com.epam.javaweb.entity.Identifiable;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * @author Ivan Andrusevich
 * @version 1.1
 * The interface is a classic implementation of Builder pattern.
 * Implementation provide functionality to create relevant domain objects from database data.
 * Builders are created in BuilderFactory
 * @see BuilderFactory#get(String)
 */
public interface Builder<T extends Identifiable> {
    /**
     * The method creates domain object based on a row from database
     *
     * @param resultSet is a row of data from relevant database table
     * @return T is a generic-type result - created object
     * @throws SQLException when something goes wrong while retrieving data from database
     */
    T build(ResultSet resultSet) throws SQLException;
}
