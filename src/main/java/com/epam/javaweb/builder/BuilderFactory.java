package com.epam.javaweb.builder;

public class BuilderFactory {
    public static Builder get(String tableName) {
        switch (tableName) {
            case "USERS": {
                return new UserBuilder();
            }
            case "MOVIES": {
                return new MovieBuilder();
            }
            case "REVIEWS": {
                return new ReviewBuilder();
            }
            case "GENRES": {
                return new GenreBuilder();
            }
            default: {
                throw new IllegalArgumentException("Unknown table " + tableName);
            }
        }
    }
}
