package com.epam.javaweb.builder;

import com.epam.javaweb.entity.Genre;

import java.sql.ResultSet;
import java.sql.SQLException;

public class GenreBuilder implements Builder<Genre> {
    @Override
    public Genre build(ResultSet resultSet) throws SQLException {
        Genre genre = new Genre();

        int id = resultSet.getInt(Genre.ID);
        genre.setId(id);

        String name = resultSet.getString(Genre.NAME);
        genre.setName(name);

        return genre;
    }
}
