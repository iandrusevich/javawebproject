package com.epam.javaweb.builder;

import com.epam.javaweb.entity.Movie;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

public class MovieBuilder implements Builder<Movie> {

    @Override
    public Movie build(ResultSet resultSet) throws SQLException {
        Movie movie = new Movie();

        int id = resultSet.getInt(Movie.ID);
        movie.setId(id);

        String title = resultSet.getString(Movie.TITLE);
        movie.setTitle(title);

        String image = resultSet.getString(Movie.IMAGE);
        movie.setImage(image);

        int genreId = resultSet.getInt(Movie.GENRE_ID);
        movie.setGenreId(genreId);

        String genre = resultSet.getString(Movie.GENRE);
        movie.setGenre(genre);

        Date premiere = resultSet.getDate(Movie.PREMIERE);
        movie.setPremiere(premiere);

        String producer = resultSet.getString(Movie.PRODUCER);
        movie.setProducer(producer);

        float averageRating = resultSet.getFloat(Movie.AVERAGE_RATING);
        movie.setAverageRating(averageRating);

        int numberOfRates = resultSet.getInt(Movie.NUMBER_OF_RATES);
        movie.setNumberOfRates(numberOfRates);
        return movie;
    }
}
