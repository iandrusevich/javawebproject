package com.epam.javaweb.builder;

import com.epam.javaweb.entity.Review;

import java.sql.ResultSet;
import java.sql.SQLException;

public class ReviewBuilder implements Builder<Review> {
    @Override
    public Review build(ResultSet resultSet) throws SQLException {
        Review review = new Review();

        int id = resultSet.getInt(Review.ID);
        review.setId(id);

        int movieId = resultSet.getInt(Review.MOVIE_ID);
        review.setMovieId(movieId);

        int userId = resultSet.getInt(Review.USER_ID);
        review.setUserId(userId);

        float rate = resultSet.getFloat(Review.RATE);
        review.setRate(rate);

        String reviewStr = resultSet.getString(Review.REVIEW);
        review.setReview(reviewStr);

        return review;
    }
}
