package com.epam.javaweb.builder;

import com.epam.javaweb.entity.User;

import java.sql.ResultSet;
import java.sql.SQLException;

public class UserBuilder implements Builder<User> {
    @Override
    public User build(ResultSet resultSet) throws SQLException {
        User user = new User();

        int id = resultSet.getInt(User.ID);
        user.setId(id);

        String login = resultSet.getString(User.LOGIN);
        user.setLogin(login);

        String passwordHash = resultSet.getString(User.PASSWORD_HASH);
        user.setPasswordHash(passwordHash);

        String name = resultSet.getString(User.NAME);
        user.setName(name);

        String surname = resultSet.getString(User.SURNAME);
        user.setSurname(surname);

        int statusId = resultSet.getInt(User.STATUS_ID);
        user.setStatusId(statusId);

        String status = resultSet.getString(User.STATUS);
        user.setStatus(status);

        boolean isActive = resultSet.getBoolean(User.IS_ACTIVE);
        user.setActive(isActive);

        boolean isAdmin = resultSet.getBoolean(User.IS_ADMIN);
        user.setAdmin(isAdmin);

        int numberOfRates = resultSet.getInt(User.NUMBER_OF_RATES);
        user.setNumberOfRates(numberOfRates);

        String language = resultSet.getString(User.LANGUAGE);
        user.setLanguage(language);

        String sessionId = resultSet.getString(User.SESSION_ID);
        user.setSessionId(sessionId);

        return user;
    }
}
