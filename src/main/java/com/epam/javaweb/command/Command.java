package com.epam.javaweb.command;

import com.epam.javaweb.exception.ServiceException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @author Ivan Andrusevich
 * @version 1.1
 * The interface is a classic implementation of Command pattern.
 * Implementations initiate relevant business logic calling based on the parameters of HTTP-request.
 * Commands are created in CommandFactory.
 * @see CommandFactory#createCommand(String)
 */
public interface Command {
    /**
     * @param request  client's HTTP-request
     * @param response client's HTTP-response
     * @return CommandResult is an object created after most of the business logic is executed.
     * Based on CommandResult Servlet defines the type of the answer to clients request: redirect or forward.
     * @throws ServiceException when something goes wrong while executing business logic.
     */
    CommandResult execute(HttpServletRequest request, HttpServletResponse response) throws ServiceException;
}
