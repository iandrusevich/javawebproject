package com.epam.javaweb.command;

import com.epam.javaweb.command.impl.*;
import com.epam.javaweb.dao.DaoFactory;
import com.epam.javaweb.entity.User;
import com.epam.javaweb.exception.ServiceException;
import com.epam.javaweb.service.GenreService;
import com.epam.javaweb.service.MovieService;
import com.epam.javaweb.service.ReviewService;
import com.epam.javaweb.service.UserService;

import java.util.Optional;

public class CommandFactory {
    private UserService userService;
    private ReviewService reviewService;
    private GenreService genreService;
    private MovieService movieService;
    private String sessionId;

    public CommandFactory(DaoFactory daoFactory, String sessionId) {
        userService = new UserService(daoFactory);
        genreService = new GenreService(daoFactory);
        reviewService = new ReviewService(daoFactory, userService);
        movieService = new MovieService(daoFactory, reviewService);
        this.sessionId = sessionId;
    }

    public Command createCommand(String commandName) throws ServiceException {
        if (commandName == null) {
            return new DefaultCommand();
        }
        Optional<User> optionalUser = userService.findBySessionId(sessionId);
        if (optionalUser.isPresent()) {
            User user = optionalUser.get();
            if (user.isAdmin()) {
                return createCommandForAdmin(commandName);
            } else {
                return createCommandForUser(commandName);
            }
        } else {
            return createCommandForAny(commandName);
        }
    }

    private Command createCommandForAdmin(String commandName) throws ServiceException {
        switch (commandName) {
            case "showAdminMovies": {
                return new ShowAdminMoviesCommand(movieService, genreService);
            }
            case "addMovie": {
                return new AddMovieCommand(movieService);
            }
            case "showUsers": {
                return new ShowUsersCommand(userService);
            }
            case "changeUserActivity": {
                return new ChangeUserActivityCommand(userService);
            }
            case "changeUserStatus":{
                return new ChangeUserStatusCommand(userService);
            }
            case "deleteReview":{
                return new DeleteReviewCommand(reviewService, movieService);
            }
            default: {
                return createCommandForAnyAuthorized(commandName);
            }
        }
    }

    private Command createCommandForUser(String commandName) throws ServiceException {
        switch (commandName) {
            case "showUserMovies": {
                return new ShowUserMoviesCommand(movieService);
            }
            case "addRate": {
                return new AddReviewCommand(reviewService, userService, movieService);
            }
            default: {
                return createCommandForAnyAuthorized(commandName);
            }
        }
    }

    private Command createCommandForAnyAuthorized(String commandName) throws ServiceException {
        switch (commandName) {
            case "login": {
                userService.deleteSessionId(sessionId);
            }
            default: {
                return createCommandForAny(commandName);
            }
        }
    }

    private Command createCommandForAny(String commandName) {
        switch (commandName) {
            case "showLoginPage": {
                return new ShowLoginPageCommand();
            }
            case "login": {
                return new LoginCommand(userService);
            }
            case "logout": {
                return new LogoutCommand(userService);
            }
            case "locale": {
                return new ChangeLocaleCommand();
            }
            default: {
                throw new IllegalArgumentException("Unknown command " + commandName);
            }
        }
    }
}
