package com.epam.javaweb.command;

public class CommandResult {
    private String page;
    private boolean isForward;

    public CommandResult(String page, boolean isForward) {
        this.page = page;
        this.isForward = isForward;
    }

    public String getPage() {
        return page;
    }

    public boolean isForward() {
        return isForward;
    }
}
