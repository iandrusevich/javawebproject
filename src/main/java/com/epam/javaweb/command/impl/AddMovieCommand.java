package com.epam.javaweb.command.impl;

import com.epam.javaweb.command.Command;
import com.epam.javaweb.command.CommandResult;
import com.epam.javaweb.exception.ServiceException;
import com.epam.javaweb.service.MovieService;
import com.epam.javaweb.util.Redirector;
import com.epam.javaweb.util.ShowResultMessageHelper;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.http.Part;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;


public class AddMovieCommand implements Command {
    private MovieService service;
    private static final String UPLOAD_DIR = "img\\posters";
    private static final String IMG_DIR = "F:\\webproject\\src\\main\\webapp\\img\\posters";

    public AddMovieCommand(MovieService service) {
        this.service = service;
    }

    @Override
    public CommandResult execute(HttpServletRequest request, HttpServletResponse response) throws ServiceException {
        try {
            String title = request.getParameter("title");
            String premiereStr = request.getParameter("premiere");
            SimpleDateFormat format = new SimpleDateFormat("yyyy-mm-dd");
            Date premiere = format.parse(premiereStr);
            String genreIdStr = request.getParameter("genreId");
            int genreId = Integer.parseInt(genreIdStr);
            String producer = request.getParameter("producer");
            Part part = request.getPart("image");
            HttpSession session = request.getSession();
            String imageName = part.getSubmittedFileName();

            String applicationPath = request.getServletContext().getRealPath("");
            String uploadFilePath = applicationPath + File.separator + UPLOAD_DIR;
            File uploadFolder = getUploadFolder(uploadFilePath);
            if (!checkIfImageNameAllowed(uploadFolder, imageName)) {
                ShowResultMessageHelper.setDisplayedMessage(session, false, "movies.movie-poster-file-name-exists.error-message");
                return Redirector.redirectToPreviousGetRequest(session);
            }
            service.addMovie(imageName, title, premiere, genreId, producer);
            String temporaryFilePath = uploadFilePath + File.separator + imageName;
            String permanentFilePath = IMG_DIR + File.separator + imageName;
            saveFile(part, permanentFilePath, temporaryFilePath);
            ShowResultMessageHelper.setDisplayedMessage(session, true, "movies.movie-added.success-message");
            return Redirector.redirectToPreviousGetRequest(session);
        } catch (ParseException | ServletException |
                IOException e) {
            throw new ServiceException(e);
        }
    }

    private File getUploadFolder(String uploadFilePath) {
        File uploadFolder = new File(uploadFilePath);
        if (!uploadFolder.exists()) {
            uploadFolder.mkdirs();
        }
        return uploadFolder;
    }

    private boolean checkIfImageNameAllowed(File uploadFolder, String imageName) {
        File[] filesInFolder = uploadFolder.listFiles();
        for (int i = 0; i < filesInFolder.length; i++) {
            String fileName = filesInFolder[i].getName();
            if (fileName.equals(imageName)) {
                return false;
            }
        }
        return true;
    }

    private void saveFile(Part part, String permanentFilePath, String temporaryFilePath) throws IOException {
        InputStream inputStream = part.getInputStream();
        File permanentFile = new File(permanentFilePath);
        File temporaryFile = new File(temporaryFilePath);
        Files.copy(inputStream, permanentFile.toPath());
        Files.copy(inputStream, temporaryFile.toPath());
    }
}
