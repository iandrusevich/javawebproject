package com.epam.javaweb.command.impl;

import com.epam.javaweb.command.Command;
import com.epam.javaweb.command.CommandResult;
import com.epam.javaweb.entity.User;
import com.epam.javaweb.exception.ServiceException;
import com.epam.javaweb.service.ReviewService;
import com.epam.javaweb.service.UserService;
import com.epam.javaweb.util.Redirector;
import com.epam.javaweb.util.ShowResultMessageHelper;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.Optional;

public class AddRateCommand implements Command {
    private ReviewService reviewService;
    private UserService userService;

    public AddRateCommand(ReviewService reviewService, UserService userService) {
        this.reviewService = reviewService;
        this.userService = userService;
    }

    @Override
    public CommandResult execute(HttpServletRequest request, HttpServletResponse response) throws ServiceException {
        String movieIdStr = request.getParameter("movieId");
        int movieId = Integer.parseInt(movieIdStr);
        HttpSession session = request.getSession();
        String sessionId = session.getId();
        Optional<User> optionalUser = userService.findBySessionId(sessionId);
        if (!optionalUser.isPresent()) {
            throw new ServiceException("User with sessionId " + sessionId + " has not been found");
        }
        User user = optionalUser.get();
        int userId = user.getId();
        String rateStr = request.getParameter("rate");
        float rate = Float.parseFloat(rateStr);
        String review = request.getParameter("review");
        reviewService.addReview(movieId, userId, rate, review);
        ShowResultMessageHelper.setDisplayedMessage(session, true, "movies.review-added.success-message");
        return Redirector.redirectToPreviousGetRequest(session);
    }
}
