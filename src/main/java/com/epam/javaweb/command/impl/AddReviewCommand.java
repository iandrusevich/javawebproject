package com.epam.javaweb.command.impl;

import com.epam.javaweb.command.Command;
import com.epam.javaweb.command.CommandResult;
import com.epam.javaweb.entity.Movie;
import com.epam.javaweb.entity.Review;
import com.epam.javaweb.entity.User;
import com.epam.javaweb.exception.ServiceException;
import com.epam.javaweb.service.MovieService;
import com.epam.javaweb.service.ReviewService;
import com.epam.javaweb.service.UserService;
import com.epam.javaweb.util.Redirector;
import com.epam.javaweb.util.ReviewValidator;
import com.epam.javaweb.util.ShowResultMessageHelper;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.List;
import java.util.Optional;

public class AddReviewCommand implements Command {
    private ReviewService reviewService;
    private UserService userService;
    private MovieService movieService;

    public AddReviewCommand(ReviewService reviewService, UserService userService, MovieService movieService) {
        this.reviewService = reviewService;
        this.userService = userService;
        this.movieService = movieService;
    }

    @Override
    public CommandResult execute(HttpServletRequest request, HttpServletResponse response) throws ServiceException {
        String movieIdStr = request.getParameter("movieId");
        int movieId = Integer.parseInt(movieIdStr);
        HttpSession session = request.getSession();
        User user = getUserBySession(session);
        int userId = user.getId();
        String rateStr = request.getParameter("rate");
        float rate = Float.parseFloat(rateStr);
        String reviewStr = request.getParameter("review");
        ReviewValidator validator = new ReviewValidator(movieService);
        if (!validator.isReviewValid(movieId, userId, rate, reviewStr)) {
            throw new ServiceException("Invalid review data");
        }
        reviewService.addReview(movieId, userId, rate, reviewStr);

        Optional<Movie> optionalMovie = movieService.getById(movieId);
        if (!optionalMovie.isPresent()) {
            throw new ServiceException("Movie with ID " + movieId + " has not been found");
        }
        Movie movie = optionalMovie.get();
        movieService.recalculateMovieRating(movie);

        float averageRating = movie.getAverageRating();
        List<Review> reviews = movie.getReviews();
        userService.recalculateReviewersStatuses(averageRating, reviews, user);

        User renewedUser = getUserBySession(session);
        session.setAttribute("user", renewedUser);
        ShowResultMessageHelper.setDisplayedMessage(session, true, "movies.review-added.success-message");
        return Redirector.redirectToPreviousGetRequest(session);
    }

    private User getUserBySession(HttpSession session) throws ServiceException {
        String sessionId = session.getId();
        Optional<User> optionalUser = userService.findBySessionId(sessionId);
        if (!optionalUser.isPresent()) {
            throw new ServiceException("User with sessionId " + sessionId + " has not been found");
        }
        return optionalUser.get();
    }
}
