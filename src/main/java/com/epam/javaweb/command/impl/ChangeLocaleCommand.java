package com.epam.javaweb.command.impl;

import com.epam.javaweb.command.Command;
import com.epam.javaweb.command.CommandResult;
import com.epam.javaweb.util.Redirector;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.Locale;
import java.util.ResourceBundle;

public class ChangeLocaleCommand implements Command {
    @Override
    public CommandResult execute(HttpServletRequest request, HttpServletResponse response) {
        String newLanguage = request.getParameter("language");
        String newLanguageCountry = request.getParameter("country");
        Locale newLocale = new Locale(newLanguage, newLanguageCountry);
        HttpSession session = request.getSession();
        session.setAttribute("locale", newLocale);
        ResourceBundle webPagesContent = ResourceBundle.getBundle("WebPagesContentBundle", newLocale);
        request.setAttribute("webPagesContent", webPagesContent);
        return Redirector.redirectToPreviousGetRequest(session);
    }
}
