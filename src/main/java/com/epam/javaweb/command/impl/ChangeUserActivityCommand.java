package com.epam.javaweb.command.impl;

import com.epam.javaweb.command.Command;
import com.epam.javaweb.command.CommandResult;
import com.epam.javaweb.entity.User;
import com.epam.javaweb.exception.ServiceException;
import com.epam.javaweb.service.UserService;
import com.epam.javaweb.util.Redirector;
import com.epam.javaweb.util.ShowResultMessageHelper;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.Optional;

public class ChangeUserActivityCommand implements Command {
    private UserService userService;

    public ChangeUserActivityCommand(UserService userService) {
        this.userService = userService;
    }

    @Override
    public CommandResult execute(HttpServletRequest request, HttpServletResponse response) throws ServiceException {
        String userIdStr = request.getParameter("userId");
        int userId = Integer.parseInt(userIdStr);
        Optional<User> optionalUser = userService.findById(userId);
        if (!optionalUser.isPresent()) {
            throw new ServiceException("User with ID " + userId + "has not been found");
        }
        User user = optionalUser.get();
        userService.changeUserActivity(user);
        HttpSession session = request.getSession();
        ShowResultMessageHelper.setDisplayedMessage(session, true, "users.user-activity-change.success-message");
        return Redirector.redirectToPreviousGetRequest(session);
    }
}
