package com.epam.javaweb.command.impl;

import com.epam.javaweb.command.Command;
import com.epam.javaweb.command.CommandResult;
import com.epam.javaweb.entity.Movie;
import com.epam.javaweb.exception.ServiceException;
import com.epam.javaweb.service.MovieService;
import com.epam.javaweb.service.ReviewService;
import com.epam.javaweb.util.Redirector;
import com.epam.javaweb.util.ShowResultMessageHelper;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.Optional;

public class DeleteReviewCommand implements Command {
    private ReviewService reviewService;
    private MovieService movieService;

    public DeleteReviewCommand(ReviewService reviewService, MovieService movieService) {
        this.reviewService = reviewService;
        this.movieService = movieService;
    }

    @Override
    public CommandResult execute(HttpServletRequest request, HttpServletResponse response) throws ServiceException {
        String reviewIdStr = request.getParameter("reviewId");
        int reviewId = Integer.parseInt(reviewIdStr);
        reviewService.deleteReviewById(reviewId);
        String movieIdStr = request.getParameter("movieId");
        int movieId = Integer.parseInt(movieIdStr);
        Optional<Movie> optionalMovie = movieService.getById(movieId);
        if (!optionalMovie.isPresent()) {
            throw new ServiceException("Movie with ID " + movieId + " has not been found");
        }
        Movie movie = optionalMovie.get();
        movieService.recalculateMovieRating(movie);
        HttpSession session = request.getSession();
        ShowResultMessageHelper.setDisplayedMessage(session, true, "movies.review-deleted.success-message");
        return Redirector.redirectToPreviousGetRequest(session);
    }
}
