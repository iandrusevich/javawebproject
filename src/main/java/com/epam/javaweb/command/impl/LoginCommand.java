package com.epam.javaweb.command.impl;

import com.epam.javaweb.command.Command;
import com.epam.javaweb.command.CommandResult;
import com.epam.javaweb.entity.User;
import com.epam.javaweb.exception.ServiceException;
import com.epam.javaweb.service.UserService;
import com.epam.javaweb.util.Redirector;
import com.epam.javaweb.util.ShowResultMessageHelper;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.Optional;


public class LoginCommand implements Command {
    private UserService userService;

    public LoginCommand(UserService service) {
        this.userService = service;
    }

    @Override
    public CommandResult execute(HttpServletRequest request, HttpServletResponse response) throws ServiceException {
        String login = request.getParameter("login");
        String password = request.getParameter("password");
        Optional<User> optionalUser = userService.login(login, password);
        HttpSession session = request.getSession();
        if (!optionalUser.isPresent()) {
            ShowResultMessageHelper.setDisplayedMessage(session, false, "login.invalid-credentials.error-message");
            return Redirector.redirectToPreviousGetRequest(session);
        }
        User user = optionalUser.get();
        if (!user.isActive()) {
            ShowResultMessageHelper.setDisplayedMessage(session, false, "login.user-inactive.error-message");
            return Redirector.redirectToPreviousGetRequest(session);
        }
        int userId = user.getId();
        String sessionId = session.getId();
        userService.addSessionId(userId, sessionId);
        session.setAttribute("user", user);
        CommandResult commandResult;
        if (user.isAdmin()) {
            commandResult = new CommandResult("?command=showAdminMovies", false);
        } else {
            commandResult = new CommandResult("?command=showUserMovies", false);
        }
        return commandResult;
    }
}

