package com.epam.javaweb.command.impl;

import com.epam.javaweb.command.Command;
import com.epam.javaweb.command.CommandResult;
import com.epam.javaweb.exception.ServiceException;
import com.epam.javaweb.service.UserService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class LogoutCommand implements Command {
    private UserService service;

    public LogoutCommand(UserService service) {
        this.service = service;
    }

    @Override
    public CommandResult execute(HttpServletRequest request, HttpServletResponse response) throws ServiceException {
        HttpSession session = request.getSession();
        String sessionId = session.getId();
        service.deleteSessionId(sessionId);
        session.setAttribute("previousGetRequest", null);
        return new CommandResult("?command=showLoginPage", false);
    }
}
