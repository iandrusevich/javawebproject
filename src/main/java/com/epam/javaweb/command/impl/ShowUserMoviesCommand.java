package com.epam.javaweb.command.impl;

import com.epam.javaweb.command.Command;
import com.epam.javaweb.command.CommandResult;
import com.epam.javaweb.entity.Movie;
import com.epam.javaweb.exception.ServiceException;
import com.epam.javaweb.service.MovieService;
import com.epam.javaweb.util.Paginator;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.List;

public class ShowUserMoviesCommand implements Command {
    private MovieService movieService;
    private static final int PAGE_SIZE = 10;
    private static final String COMMAND_URL = "?command=showUserMovies";

    public ShowUserMoviesCommand(MovieService movieService) {
        this.movieService = movieService;
    }

    @Override
    public CommandResult execute(HttpServletRequest request, HttpServletResponse response) throws ServiceException {
        String currentPageStr = request.getParameter("page");
        int currentPage = (currentPageStr == null) ? 1 : Integer.parseInt(currentPageStr);
        List<Movie> allMovies = movieService.getAllMovies();
        Paginator paginator = new Paginator(request, response);
        paginator.setPaginationIntoRequest(allMovies, currentPage, PAGE_SIZE, COMMAND_URL);
        List<Movie> selectedMovies = paginator.getItemsForCurrentPage(allMovies, currentPage, PAGE_SIZE);
        request.setAttribute("movies", selectedMovies);

        HttpSession session = request.getSession();
        session.setAttribute("previousGetRequest", COMMAND_URL + "&page=" + currentPage);
        return new CommandResult("WEB-INF/user.movies.jsp", true);
    }
}
