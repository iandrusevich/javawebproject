package com.epam.javaweb.command.impl;

import com.epam.javaweb.command.Command;
import com.epam.javaweb.command.CommandResult;
import com.epam.javaweb.entity.User;
import com.epam.javaweb.exception.ServiceException;
import com.epam.javaweb.service.UserService;
import com.epam.javaweb.util.Paginator;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.List;

public class ShowUsersCommand implements Command {
    private UserService service;
    private static final int PAGE_SIZE = 10;
    private static final String COMMAND_URL = "?command=showUsers";

    public ShowUsersCommand(UserService service) {
        this.service = service;
    }

    @Override
    public CommandResult execute(HttpServletRequest request, HttpServletResponse response) throws ServiceException {
        String currentPageStr = request.getParameter("page");
        int currentPage = (currentPageStr == null) ? 1 : Integer.parseInt(currentPageStr);
        List<User> allNonAdminUsers = service.getAllNonAdminUsers();
        Paginator paginator = new Paginator(request, response);
        paginator.setPaginationIntoRequest(allNonAdminUsers, currentPage, PAGE_SIZE, COMMAND_URL);
        List<User> nonAdminUsersForCurrentPage = paginator.getItemsForCurrentPage(allNonAdminUsers, currentPage, PAGE_SIZE);
        request.setAttribute("nonAdminUsers", nonAdminUsersForCurrentPage);
        HttpSession session = request.getSession();
        session.setAttribute("previousGetRequest", COMMAND_URL + "&page=" + currentPage);
        return new CommandResult("WEB-INF/admin.users.jsp", true);
    }
}
