package com.epam.javaweb.dao;

import com.epam.javaweb.builder.Builder;
import com.epam.javaweb.builder.BuilderFactory;
import com.epam.javaweb.entity.Identifiable;
import com.epam.javaweb.exception.DaoException;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public abstract class AbstractDao<T extends Identifiable> implements Dao<T> {
    private Connection connection;
    private String table = getTableName();
    private Builder<T> builder = BuilderFactory.get(table);

    public AbstractDao(Connection connection) {
        this.connection = connection;
    }

    protected List<T> executeQuery(String query, Builder<T> builder, String... params) throws DaoException {
        try {
            PreparedStatement statement = connection.prepareStatement(query);
            for (int i = 0; i < params.length; i++) {
                statement.setString(i + 1, params[i]);
            }
            ResultSet resultSet = statement.executeQuery();
            List<T> entities = new ArrayList<>();
            while (resultSet.next()) {
                T entity = builder.build(resultSet);
                entities.add(entity);
            }
            return entities;
        } catch (SQLException e) {
            throw new DaoException(e);
        }
    }

    @Override
    public List<T> getAll() throws DaoException {
        return executeQuery("select * from " + table, builder);
    }

    @Override
    public Optional<T> getById(Long id) throws DaoException {
        String idStr = id.toString();
        return executeForSingleResult("select * from " + table + "where ID = " + idStr + ";", builder);
    }

    @Override
    public void removeById(Long id) throws DaoException {
        String idStr = String.valueOf(id);
        int numberOfRowsAffected = executeUpdate("delete from " + table + " where ID = ?;", idStr);
        if (numberOfRowsAffected == 0) {
            throw new DaoException("No rows have been affected deleting the item with ID " + id + " from table " + table);
        }
    }

    protected Optional<T> executeForSingleResult(String query, Builder<T> builder, String... params) throws DaoException {
        List<T> items = executeQuery(query, builder, params);
        if (items.size() == 1) {
            return Optional.of(items.get(0));
        } else {
            return Optional.empty();
        }
    }

    protected int executeUpdate(String query, String... params) throws DaoException {
        int numberOfRowsAffected;
        try {
            PreparedStatement statement = connection.prepareStatement(query);
            for (int i = 0; i < params.length; i++) {
                statement.setString(i + 1, params[i]);
            }
            numberOfRowsAffected = statement.executeUpdate();
        } catch (SQLException e) {
            throw new DaoException(e);
        }
        return numberOfRowsAffected;
    }

    protected abstract String getTableName();
}


