package com.epam.javaweb.dao;

import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.Semaphore;
import java.util.concurrent.atomic.AtomicReference;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class ConnectionPool {
    private final static int MAX_NUMBER_OF_CONNECTIONS = 5;
    private static AtomicReference<ConnectionPool> instance = new AtomicReference<>();
    private final static Lock LOCK = new ReentrantLock();
    private BlockingQueue<Connection> connectionPool = new ArrayBlockingQueue<>(MAX_NUMBER_OF_CONNECTIONS);
    private BlockingQueue<Connection> issuedConnections = new ArrayBlockingQueue<>(MAX_NUMBER_OF_CONNECTIONS);
    private Semaphore semaphore = new Semaphore(MAX_NUMBER_OF_CONNECTIONS, true);
    private Properties properties = new Properties();

    private ConnectionPool() {
        try {
            InputStream inputStream = getClass().getClassLoader().getResourceAsStream("db.properties");
            properties.load(inputStream);
        } catch (IOException e){
            throw new IllegalStateException(e);
        }
        for (int i = 0; i < MAX_NUMBER_OF_CONNECTIONS; i++) {
            Connection connection = createConnection();
            connectionPool.add(connection);
        }
    }

    public static ConnectionPool getInstance() {
        if (instance.get() == null) {
            try {
                LOCK.lock();
                if (instance.get() == null) {
                    instance.set(new ConnectionPool());
                }
            } finally {
                LOCK.unlock();
            }
        }
        return instance.get();
    }

    public Connection getConnection() {
        try {
            semaphore.acquire();
        } catch (InterruptedException e) {
            throw new IllegalStateException(e);
        }
        Connection connection = connectionPool.poll();
        issuedConnections.add(connection);
        return connection;
    }

    public void releaseConnection(Connection connection) throws SQLException {
        if (issuedConnections.contains(connection)) {
            connection.rollback();
            issuedConnections.remove(connection);
            connectionPool.add(connection);
            semaphore.release();
        }
    }

    private Connection createConnection() {
        try {
            DriverManager.registerDriver(new com.mysql.jdbc.Driver());
            String dbUrl = properties.getProperty("url");
            Connection connection = DriverManager.getConnection(dbUrl, properties);
            connection.setAutoCommit(false);
            return new ProxyConnection(connection, this);
        } catch (SQLException e) {
            throw new IllegalStateException(e);
        }
    }
}
