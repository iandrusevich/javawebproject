package com.epam.javaweb.dao;

import com.epam.javaweb.entity.Identifiable;
import com.epam.javaweb.exception.DaoException;

import java.util.List;
import java.util.Optional;

/**
 * @author Ivan Andrusevich
 * @version 1.1
 * The interface is a classic implementation of Dao pattern.
 * Implementations of the interface provide functionality to execute CRUD-operations towards relevant entities.
 */
public interface Dao<T extends Identifiable> {
    /**
     * The method is used to get a single entity based on its unique identifier
     *
     * @param id represents the unique entity identifier which is used to retrieve the entity
     * @return Optional<T> represents generic-type entity-object retrieved from a storage,
     * wrapped into Optional to provide safe return of null
     * @throws DaoException when something goes wrong while retrieving the entity
     */
    Optional<T> getById(Long id) throws DaoException;

    /**
     * The method is used to get ALL the entities of a certain type from a storage
     *
     * @return List<T> represents generic-type list of entity-objects retrieved from a storage,
     * @throws DaoException when something goes wrong while retrieving the entity
     */
    List<T> getAll() throws DaoException;

    /**
     * The method is used to save object as an entity into a storage
     * @param item represents object of a type specified by the interface implementation,
     * which is saved (created or updated) into a storage
     * @throws DaoException when something goes wrong while retrieving the entity
     */
    void save(T item) throws DaoException;

    /**
     * The method is used to delete object entity from a storage
     * @param id represents the unique entity identifier which is used to identify entity to be deleted
     * @throws DaoException when something goes wrong while retrieving the entity
     */
    void removeById(Long id) throws DaoException;
}
