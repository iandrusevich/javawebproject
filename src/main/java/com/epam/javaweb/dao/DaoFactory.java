package com.epam.javaweb.dao;

import java.sql.Connection;

public class DaoFactory {
    private Connection connection;

    public DaoFactory(Connection connection) {
        this.connection = connection;
    }

    public UserDao getUserDao() {
        return new UserDaoImpl(connection);
    }

    public MovieDao getMovieDao() {
        return new MovieDaoImpl(connection);
    }

    public GenreDao getGenreDao() {
        return new GenreDaoImpl(connection);
    }

    public ReviewDao getReviewDao() {
        return new ReviewDaoImpl(connection);
    }
}
