package com.epam.javaweb.dao;

import com.epam.javaweb.entity.Genre;

/**
 * @author Ivan Andrusevich
 * @version 1.1
 * The interface extends {@link Dao} to provide methods specific to work with such an entity as Genre
 */
public interface GenreDao extends Dao<Genre> {
}
