package com.epam.javaweb.dao;

import com.epam.javaweb.entity.Genre;
import com.epam.javaweb.exception.DaoException;

import java.sql.Connection;

public class GenreDaoImpl extends AbstractDao<Genre> implements GenreDao {
    private static final String ADD_GENRE = "insert into GENRES (NAME) values (?);";

    public GenreDaoImpl(Connection connection) {
        super(connection);
    }

    @Override
    protected String getTableName() {
        return "GENRES";
    }

    @Override
    public void save(Genre item) throws DaoException {
        String name = item.getName();
        int numberOfRowsAffected = super.executeUpdate(ADD_GENRE, name);
        if (numberOfRowsAffected == 0) {
            throw new DaoException("No rows have been affected while saving the genre");
        }
    }
}
