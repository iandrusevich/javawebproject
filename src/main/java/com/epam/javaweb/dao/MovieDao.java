package com.epam.javaweb.dao;

import com.epam.javaweb.entity.Movie;
import com.epam.javaweb.exception.DaoException;

/**
 * @author Ivan Andrusevich
 * @version 1.1
 * The interface extends {@link Dao} to provide methods specific to work with such an entity as Movie
 */
public interface MovieDao extends Dao<Movie> {
    /**
     * The method is used to add such parameter as average rating to a Movie-entity in a storage
     *
     * @param id     represents the unique entity identifier which is used to identify the entity
     * @param rating represents the rating to be added to a Movie-entity
     * @throws DaoException when something goes wrong while retrieving the entity
     */
    void addAverageRating(Long id, Float rating) throws DaoException;

    /**
     * The method is used to add such parameter as number of rates to a Movie-entity in a storage
     *
     * @param id            represents the unique entity identifier which is used to identify the entity
     * @param numberOfRates represents the number of rates to be added to a Movie-entity
     * @throws DaoException when something goes wrong while retrieving the entity
     */
    void addNumberOfRates(Long id, Integer numberOfRates) throws DaoException;
}
