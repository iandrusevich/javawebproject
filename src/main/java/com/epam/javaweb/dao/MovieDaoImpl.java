package com.epam.javaweb.dao;

import com.epam.javaweb.builder.MovieBuilder;
import com.epam.javaweb.entity.Movie;
import com.epam.javaweb.exception.DaoException;

import java.sql.Connection;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Optional;

public class MovieDaoImpl extends AbstractDao<Movie> implements MovieDao {

    private static final String FIND_ALL_MOVIES = "select M.*, G.NAME AS GENRE from MOVIES as M inner join GENRES as G on M.GENRE_ID = G.ID order by ID;";
    private static final String FIND_BY_ID = "select M.*, G.NAME AS GENRE from MOVIES as M inner join GENRES as G on M.GENRE_ID = G.ID where M.ID = ?;";
    private static final String ADD_MOVIE = "insert into MOVIES (TITLE, IMAGE, GENRE_ID, PREMIERE, PRODUCER) values (?, ?, ?, ?, ?);";
    private static final String ADD_AVERAGE_RATING = "update MOVIES set AVERAGE_RATING = ? where ID = ?;";
    private static final String ADD_NUMBER_OF_RATES = "update MOVIES set NUMBER_OF_RATES = ? where ID = ?;";

    public MovieDaoImpl(Connection connection) {
        super(connection);
    }

    @Override
    protected String getTableName() {
        return "MOVIES";
    }

    @Override
    public Optional<Movie> getById(Long id) throws DaoException {
        String idStr = id.toString();
        return executeForSingleResult(FIND_BY_ID, new MovieBuilder(), idStr);
    }

    @Override
    public List<Movie> getAll() throws DaoException {
        return executeQuery(FIND_ALL_MOVIES, new MovieBuilder());
    }

    @Override
    public void save(Movie item) throws DaoException {
        String title = item.getTitle();
        String image = item.getImage();
        int genreId = item.getGenreId();
        String genreIdStr = Integer.toString(genreId);
        Date premiere = item.getPremiere();
        SimpleDateFormat format = new SimpleDateFormat("yyyy-mm-dd");
        String premiereStr = format.format(premiere);
        String producer = item.getProducer();
        int numberOfRowsAffected = super.executeUpdate(ADD_MOVIE, title, image, genreIdStr, premiereStr, producer);
        if (numberOfRowsAffected == 0) {
            throw new DaoException("No rows have been affected while saving a movie");
        }
    }

    @Override
    public void addAverageRating(Long id, Float rating) throws DaoException {
        String idStr = id.toString();
        String ratingStr = rating.toString();
        int numberOfRowsAffected = executeUpdate(ADD_AVERAGE_RATING, ratingStr, idStr);
        if (numberOfRowsAffected == 0) {
            throw new DaoException("No rows have been affected while adding rating to movie with ID" + id);
        }
    }

    @Override
    public void addNumberOfRates(Long id, Integer numberOfRates) throws DaoException {
        String idStr = id.toString();
        String numberOfRatesStr = numberOfRates.toString();
        int numberOfRowsAffected = executeUpdate(ADD_NUMBER_OF_RATES, numberOfRatesStr, idStr);
        if (numberOfRowsAffected == 0) {
            throw new DaoException("No rows have been affected while adding number of rates to movie with ID" + id);
        }
    }
}
