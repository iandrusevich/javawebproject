package com.epam.javaweb.dao;

import com.epam.javaweb.entity.Review;
import com.epam.javaweb.exception.DaoException;

import java.util.List;

/**
 * @author Ivan Andrusevich
 * @version 1.1
 * The interface extends {@link Dao} to provide methods specific to work with such an entity as Review
 */
public interface ReviewDao extends Dao<Review> {
    /**
     * The method is used to get Review-entity by the reviewed Movie-entity
     *
     * @param movieId represents the unique Movie-entity identifier which is used to find all the Reviews to this Movie
     * @throws DaoException when something goes wrong while retrieving the entity
     */
    List<Review> getByMovieId(String movieId) throws DaoException;

    /**
     * The method is used to get Review-entity by the reviewed Movie-entity
     *
     * @param userId represents the unique User-entity identifier which is used to find all the Reviews this User has left
     * @throws DaoException when something goes wrong while retrieving the entity
     */
    List<Review> getByUserId(String userId) throws DaoException;
}
