package com.epam.javaweb.dao;

import com.epam.javaweb.builder.ReviewBuilder;
import com.epam.javaweb.entity.Review;
import com.epam.javaweb.exception.DaoException;

import java.sql.Connection;
import java.util.List;

public class ReviewDaoImpl extends AbstractDao<Review> implements ReviewDao {
    private final static String GET_BY_MOVIE_ID = "select * from REVIEWS where MOVIE_ID = ?;";
    private final static String GET_BY_USER_ID = "select * from REVIEWS where USER_ID = ?;";
    private final static String ADD_REVIEW = "insert into REVIEWS (MOVIE_ID, USER_ID, RATE, REVIEW) values (?, ?, ?, ?);";

    public ReviewDaoImpl(Connection connection) {
        super(connection);
    }

    @Override
    protected String getTableName() {
        return "REVIEWS";
    }

    @Override
    public void save(Review review) throws DaoException {
        int movieId = review.getMovieId();
        String movieIdStr = "" + movieId;
        int userId = review.getUserId();
        String userIdStr = "" + userId;
        float rate = review.getRate();
        String rateStr = "" + rate;
        String reviewStr = review.getReview();
        int numberOfRowsAffected = super.executeUpdate(ADD_REVIEW, movieIdStr, userIdStr, rateStr, reviewStr);
        if (numberOfRowsAffected == 0) {
            throw new DaoException("No rows have been affected while saving the review");
        }
    }

    @Override
    public List<Review> getByMovieId(String movieId) throws DaoException {
        return super.executeQuery(GET_BY_MOVIE_ID, new ReviewBuilder(), movieId);
    }

    @Override
    public List<Review> getByUserId(String userId) throws DaoException {
        return super.executeQuery(GET_BY_USER_ID, new ReviewBuilder(), userId);
    }
}
