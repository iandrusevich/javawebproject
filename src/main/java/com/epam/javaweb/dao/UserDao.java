package com.epam.javaweb.dao;

import com.epam.javaweb.entity.User;
import com.epam.javaweb.exception.DaoException;

import java.util.Optional;

/**
 * @author Ivan Andrusevich
 * @version 1.1
 * The interface extends {@link Dao} to provide methods specific to work with such an entity as User
 */
public interface UserDao extends Dao<User> {
    /**
     * The method is used to get User-entity by the login/password pair.
     * The result is wrapped into Optional to provide safe return of null
     *
     * @param login    represents login
     * @param password represents password
     * @throws DaoException when something goes wrong while retrieving the entity
     */
    Optional<User> findUserByLoginAndPassword(String login, String password) throws DaoException;

    /**
     * The method is used to get User-entity by session ID started by this User.
     * The result is wrapped into Optional to provide safe return of null
     *
     * @param sessionId represents th session ID
     * @throws DaoException when something goes wrong while retrieving the entity
     */
    Optional<User> findUserBySessionId(String sessionId) throws DaoException;

    /**
     * The method is used to set  session ID to a User-entity.
     *
     * @param userId    represents the User ID
     * @param sessionId represents th session ID
     * @throws DaoException when something goes wrong while retrieving the entity
     */
    void addSessionId(Long userId, String sessionId) throws DaoException;

    /**
     * The method is used to remove session ID from a User-entity.
     *
     * @param sessionId represents the session ID
     * @throws DaoException when something goes wrong while retrieving the entity
     */
    void deleteSessionId(String sessionId) throws DaoException;

    /**
     * The method is used to change switch on/off the User activity.
     *
     * @param userId     represents the User ID
     * @param isActivate represents whether activity should be switched on (if true) or off (if false).
     * @throws DaoException when something goes wrong while retrieving the entity
     */
    void changeUserActivityById(Long userId, boolean isActivate) throws DaoException;

    /**
     * The method is used to upgrade or downgrade the User's status.
     *
     * @param userId     represents the User ID
     * @param isIncrease represents whether status should be increased (if true) or decreased (if false).
     * @throws DaoException when something goes wrong while retrieving the entity
     */
    void changeUserStatusById(Long userId, boolean isIncrease) throws DaoException;
}
