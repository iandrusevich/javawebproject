package com.epam.javaweb.dao;


import com.epam.javaweb.builder.UserBuilder;
import com.epam.javaweb.entity.User;
import com.epam.javaweb.exception.DaoException;

import java.sql.Connection;
import java.util.List;
import java.util.Optional;

public class UserDaoImpl extends AbstractDao<User> implements UserDao {

    private static final String ADD_USER = "insert into USERS (LOGIN, PASSWORD_HASH, NAME, SURNAME, STATUS_ID, IS_ACTIVE, IS_ADMIN) values (?, ?, ?, ?, ?, ?, ?);";
    private static final String GET_ALL = "select U.*, S.STATUS from USERS as U left join STATUSES as S on U.STATUS_ID = S.ID ORDER BY ID;";
    private static final String FIND_BY_LOGIN_AND_PASSWORD = "select U.*, S.STATUS from USERS as U left join STATUSES as S on U.STATUS_ID = S.ID where U.LOGIN = ? and U.PASSWORD_HASH = md5(?);";
    private static final String FIND_BY_ID = "select U.*, S.STATUS from USERS as U left join STATUSES as S on U.STATUS_ID = S.ID where U.ID = ?;";
    private static final String FIND_BY_SESSION_ID = "select U.*, S.STATUS from USERS as U left join STATUSES as S on U.STATUS_ID = S.ID where SESSION_ID = ?";
    private static final String ADD_SESSION_ID = "update USERS set SESSION_ID = ? where ID = ?;";
    private static final String DELETE_SESSION_ID = "update USERS set SESSION_ID = null where SESSION_ID = ?;";
    private static final String ACTIVATE_BY_ID = "update USERS set IS_ACTIVE = true where ID = ?";
    private static final String DEACTIVATE_BY_ID = "update USERS set IS_ACTIVE = false where ID = ?";
    private static final String INCREASE_STATUS_BY_ID = "update USERS set STATUS_ID = STATUS_ID + 1 where ID = ?;";
    private static final String DECREASE_STATUS_BY_ID = "update USERS set STATUS_ID = STATUS_ID - 1 where ID = ?;";


    public UserDaoImpl(Connection connection) {
        super(connection);
    }

    @Override
    public Optional<User> findUserByLoginAndPassword(String login, String password) throws DaoException {
        return executeForSingleResult(FIND_BY_LOGIN_AND_PASSWORD, new UserBuilder(), login, password);
    }

    @Override
    public Optional<User> findUserBySessionId(String sessionId) throws DaoException {
        return executeForSingleResult(FIND_BY_SESSION_ID, new UserBuilder(), sessionId);
    }

    @Override
    public void addSessionId(Long id, String sessionId) throws DaoException {
        String idStr = id.toString();
        int numberOfRowsAffected;
        numberOfRowsAffected = super.executeUpdate(ADD_SESSION_ID, sessionId, idStr);
        if (numberOfRowsAffected == 0) {
            throw new DaoException("No rows have been affected while adding the session ID");
        }
    }

    @Override
    public void deleteSessionId(String sessionId) throws DaoException {
        int numberOfRowsAffected;
        numberOfRowsAffected = super.executeUpdate(DELETE_SESSION_ID, sessionId);
        if (numberOfRowsAffected == 0) {
            throw new DaoException("No rows have been affected while adding the session ID");
        }
    }

    @Override
    protected String getTableName() {
        return "USERS";
    }

    @Override
    public List<User> getAll() throws DaoException {
        return super.executeQuery(GET_ALL, new UserBuilder());
    }

    @Override
    public Optional<User> getById(Long id) throws DaoException {
        String idStr = id.toString();
        return executeForSingleResult(FIND_BY_ID, new UserBuilder(), idStr);
    }

    @Override
    public void save(User item) throws DaoException {
        String login = item.getLogin();
        String passwordHash = item.getPasswordHash();
        String name = item.getName();
        String surname = item.getSurname();
        int statusId = item.getStatusId();
        String statusIdStr = Integer.toString(statusId);
        boolean isActive = item.isActive();
        String isActiveStr = Boolean.toString(isActive);
        boolean isAdmin = item.isAdmin();
        String isAdminStr = Boolean.toString(isAdmin);
        int numberOfRowsAffected = super.executeUpdate(ADD_USER, login, passwordHash, name, surname, statusIdStr, isActiveStr, isAdminStr);
        if (numberOfRowsAffected == 0) {
            throw new DaoException("No rows have been affected while saving the user");
        }
    }

    @Override
    public void changeUserActivityById(Long id, boolean isActivate) throws DaoException {
        String idStr = id.toString();
        int numberOfRowsAffected;
        if (isActivate) {
            numberOfRowsAffected = executeUpdate(ACTIVATE_BY_ID, idStr);
        } else {
            numberOfRowsAffected = executeUpdate(DEACTIVATE_BY_ID, idStr);
        }
        if (numberOfRowsAffected == 0) {
            throw new DaoException("No rows have been affected while changing user activity for the user with ID" + idStr);
        }
    }

    @Override
    public void changeUserStatusById(Long id, boolean isIncrease) throws DaoException {
        String idStr = id.toString();
        int numberOfRowsAffected;
        if (isIncrease) {
            numberOfRowsAffected = executeUpdate(INCREASE_STATUS_BY_ID, idStr);
        } else {
            numberOfRowsAffected = executeUpdate(DECREASE_STATUS_BY_ID, idStr);
        }
        if (numberOfRowsAffected == 0) {
            throw new DaoException("No rows have been affected while changing user status for the user with ID" + idStr);
        }
    }
}
