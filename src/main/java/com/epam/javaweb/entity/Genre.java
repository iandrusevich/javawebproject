package com.epam.javaweb.entity;

public class Genre implements Identifiable {
    private int id;
    private String name;

    public static final String ID = "ID";
    public static final String NAME = "NAME";

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Genre{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }
}
