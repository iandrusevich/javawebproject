package com.epam.javaweb.entity;

/**
 * @author Ivan Andrusevich
 * @version 1.1
 * The interface is a marker used to mark Entity-classes.
 * This identification is used in {@link com.epam.javaweb.dao.Dao} implementations:
 * Dao's work only with classed marked as Identifiable
 */
public interface Identifiable {
}
