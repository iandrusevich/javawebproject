package com.epam.javaweb.entity;

import java.util.Date;
import java.util.List;

public class Movie implements Identifiable {
    private int id;
    private String title;
    private String image;
    private int genreId;
    private String genre;
    private Date premiere;
    private String producer;
    private float averageRating;
    private int numberOfRates;
    private List<Review> reviews;

    public static final String ID = "ID";
    public static final String TITLE = "TITLE";
    public static final String IMAGE = "IMAGE";
    public static final String GENRE_ID = "GENRE_ID";
    public static final String GENRE = "GENRE";
    public static final String PREMIERE = "PREMIERE";
    public static final String PRODUCER = "PRODUCER";
    public static final String AVERAGE_RATING = "AVERAGE_RATING";
    public static final String NUMBER_OF_RATES = "NUMBER_OF_RATES";


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getImage(){
        return image;
    }

    public void setImage(String image){
        this.image = image;
    }

    public int getGenreId() {
        return genreId;
    }

    public void setGenreId(int genreId) {
        this.genreId = genreId;
    }

    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    public Date getPremiere() {
        return premiere;
    }

    public void setPremiere(Date premiere) {
        this.premiere = premiere;
    }

    public String getProducer() {
        return producer;
    }

    public void setProducer(String producer) {
        this.producer = producer;
    }

    public float getAverageRating() {
        return averageRating;
    }

    public void setAverageRating(float averageRating) {
        this.averageRating = averageRating;
    }

    public int getNumberOfRates() {
        return numberOfRates;
    }

    public void setNumberOfRates(int numberOfRates) {
        this.numberOfRates = numberOfRates;
    }

    public List<Review> getReviews() {
        return reviews;
    }

    public void setReviews(List<Review> reviews) {
        this.reviews = reviews;
    }

    @Override
    public String toString() {
        return "Movie{" +
                "id=" + id +
                ", title='" + title + '\'' +
                ", genreId=" + genreId +
                ", genre='" + genre + '\'' +
                ", premiere=" + premiere +
                ", producer='" + producer + '\'' +
                ", averageRating=" + averageRating +
                ", numberOfRates=" + numberOfRates +
                ", reviews=" + reviews +
                '}';
    }
}
