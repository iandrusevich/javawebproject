package com.epam.javaweb.entity;

public class Review implements Identifiable {
    private int id;
    private int movieId;
    private int userId;
    private float rate;
    private String review;
    private User user;

    public static final String ID = "ID";
    public static final String MOVIE_ID = "MOVIE_ID";
    public static final String USER_ID = "USER_ID";
    public static final String RATE = "RATE";
    public static final String REVIEW = "REVIEW";

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getMovieId() {
        return movieId;
    }

    public void setMovieId(int movieId) {
        this.movieId = movieId;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public float getRate() {
        return rate;
    }

    public void setRate(float rate) {
        this.rate = rate;
    }

    public String getReview() {
        return review;
    }

    public void setReview(String review) {
        this.review = review;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    @Override
    public String toString() {
        return "Review{" +
                "id=" + id +
                ", movieId=" + movieId +
                ", userId=" + userId +
                ", rate=" + rate +
                ", review='" + review + '\'' +
                ", user=" + user +
                '}';
    }
}
