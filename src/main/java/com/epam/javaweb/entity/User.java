package com.epam.javaweb.entity;

public class User implements Identifiable {
    private int id;
    private String login;
    private String passwordHash;
    private String name;
    private String surname;
    private int statusId;
    private String status;
    private boolean isActive;
    private boolean isAdmin;
    private int numberOfRates;
    private String language;
    private String sessionId;

    public static final String ID = "ID";
    public static final String LOGIN = "LOGIN";
    public static final String PASSWORD_HASH = "PASSWORD_HASH";
    public static final String NAME = "NAME";
    public static final String SURNAME = "SURNAME";
    public static final String STATUS_ID = "STATUS_ID";
    public static final String STATUS = "STATUS";
    public static final String IS_ACTIVE = "IS_ACTIVE";
    public static final String IS_ADMIN = "IS_ADMIN";
    public static final String NUMBER_OF_RATES = "NUMBER_OF_RATES";
    public static final String LANGUAGE = "LANGUAGE";
    public static final String SESSION_ID = "SESSION_ID";

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPasswordHash() {
        return passwordHash;
    }

    public void setPasswordHash(String passwordHash) {
        this.passwordHash = passwordHash;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public int getStatusId() {
        return statusId;
    }

    public void setStatusId(int statusId) {
        this.statusId = statusId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public boolean isActive() {
        return isActive;
    }

    public void setActive(boolean active) {
        isActive = active;
    }

    public boolean isAdmin() {
        return isAdmin;
    }

    public void setAdmin(boolean admin) {
        isAdmin = admin;
    }

    public int getNumberOfRates() {
        return numberOfRates;
    }

    public void setNumberOfRates(int numberOfRates) {
        this.numberOfRates = numberOfRates;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getSessionId() {
        return sessionId;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", login='" + login + '\'' +
                ", passwordHash='" + passwordHash + '\'' +
                ", name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", statusId=" + statusId +
                ", isActive=" + isActive +
                ", isAdmin=" + isAdmin +
                ", numberOfRates=" + numberOfRates +
                ", language='" + language + '\'' +
                ", sessionId=" + sessionId +
                '}';
    }
}
