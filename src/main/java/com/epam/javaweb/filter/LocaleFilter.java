package com.epam.javaweb.filter;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.Locale;
import java.util.ResourceBundle;

public class LocaleFilter implements Filter {

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest) servletRequest;
        HttpSession session = request.getSession();
        Locale locale = (Locale) session.getAttribute("locale");
        if (locale == null) {
            locale = request.getLocale();
            session.setAttribute("locale", locale);
        }
        ResourceBundle webPagesContent = ResourceBundle.getBundle("WebPagesContentBundle", locale);
        ResourceBundle messages = ResourceBundle.getBundle("MessagesBundle", locale);
        request.setAttribute("webPagesContent", webPagesContent);
        request.setAttribute("messages", messages);

        filterChain.doFilter(servletRequest, servletResponse);
    }
}
