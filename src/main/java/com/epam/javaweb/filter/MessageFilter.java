package com.epam.javaweb.filter;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.io.IOException;

public class MessageFilter implements Filter {

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest req = (HttpServletRequest) servletRequest;
        HttpSession session = req.getSession();
        Boolean isErrorMessageDisplayed = (Boolean) session.getAttribute("isErrorMessageDisplayed");
        if (isErrorMessageDisplayed != null) {
            if (isErrorMessageDisplayed) {
                session.removeAttribute("errorMessage");
                session.removeAttribute("isErrorMessageDisplayed");
            } else {
                session.setAttribute("isErrorMessageDisplayed", true);
            }
        }
        Boolean isSuccessMessageDisplayed = (Boolean) session.getAttribute("isSuccessMessageDisplayed");
        if (isSuccessMessageDisplayed != null) {
            if (isSuccessMessageDisplayed) {
                session.removeAttribute("successMessage");
                session.removeAttribute("isSuccessMessageDisplayed");
            } else {
                session.setAttribute("isSuccessMessageDisplayed", true);
            }
        }
        filterChain.doFilter(servletRequest, servletResponse);
    }
}
