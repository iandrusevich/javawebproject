package com.epam.javaweb.service;

import com.epam.javaweb.dao.DaoFactory;
import com.epam.javaweb.dao.GenreDao;
import com.epam.javaweb.entity.Genre;
import com.epam.javaweb.exception.DaoException;
import com.epam.javaweb.exception.ServiceException;

import java.util.List;

public class GenreService {
    private GenreDao genreDao;

    public GenreService(DaoFactory daoFactory) {
        genreDao = daoFactory.getGenreDao();
    }

    public List<Genre> getGenres() throws ServiceException {
        try {
            return genreDao.getAll();
        } catch (DaoException e) {
            throw new ServiceException(e);
        }
    }
}
