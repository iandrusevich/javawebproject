package com.epam.javaweb.service;

import com.epam.javaweb.dao.DaoFactory;
import com.epam.javaweb.dao.MovieDao;
import com.epam.javaweb.entity.Movie;
import com.epam.javaweb.entity.Review;
import com.epam.javaweb.exception.DaoException;
import com.epam.javaweb.exception.ServiceException;

import java.util.Date;
import java.util.List;
import java.util.Optional;

public class MovieService {
    private MovieDao movieDao;
    private ReviewService reviewService;

    public MovieService(DaoFactory daoFactory, ReviewService reviewService) {
        movieDao = daoFactory.getMovieDao();
        this.reviewService = reviewService;
    }

    public List<Movie> getAllMovies() throws ServiceException {
        try {
            List<Movie> movies = movieDao.getAll();
            fetchReviewsToMovies(movies);
            return movies;
        } catch (DaoException e) {
            throw new ServiceException(e);
        }
    }

    public Optional<Movie> getById(int id) throws ServiceException {
        try {
            Optional<Movie> optionalMovie = movieDao.getById((long) id);
            if (optionalMovie.isPresent()) {
                Movie movie = optionalMovie.get();
                fetchReviewsToMovie(movie);
                return Optional.of(movie);
            } else {
                return optionalMovie;
            }
        } catch (DaoException e) {
            throw new ServiceException(e);
        }
    }

    public void addMovie(String image, String title, Date premiere, int genreId, String producer) throws ServiceException {
        Movie movie = new Movie();
        movie.setImage(image);
        movie.setTitle(title);
        movie.setPremiere(premiere);
        movie.setGenreId(genreId);
        movie.setProducer(producer);
        try {
            movieDao.save(movie);
        } catch (DaoException e) {
            throw new ServiceException(e);
        }
    }

    public void recalculateMovieRating(Movie movie) throws ServiceException {
        int movieId = movie.getId();
        List<Review> reviews = reviewService.getReviewsByMovieId(movieId);
        float sumOfRates = 0;
        for (Review review : reviews) {
            sumOfRates += review.getRate();
        }
        int numberOfReviews = reviews.size();
        float averageRate = (numberOfReviews == 0) ? 0 : sumOfRates / numberOfReviews;
        try {
            movieDao.addAverageRating((long) movieId, averageRate);
            movieDao.addNumberOfRates((long) movieId, numberOfReviews);
        } catch (DaoException e) {
            throw new ServiceException(e);
        }
    }

    private void fetchReviewsToMovies(List<Movie> movies) throws ServiceException {
        for (Movie movie : movies) {
            fetchReviewsToMovie(movie);
        }
    }

    private void fetchReviewsToMovie(Movie movie) throws ServiceException {
        if (movie != null) {
            int movieId = movie.getId();
            List<Review> reviewsForMovie = reviewService.getReviewsByMovieId(movieId);
            movie.setReviews(reviewsForMovie);
        }

    }
}
