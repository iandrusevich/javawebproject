package com.epam.javaweb.service;

import com.epam.javaweb.dao.DaoFactory;
import com.epam.javaweb.dao.ReviewDao;
import com.epam.javaweb.entity.Review;
import com.epam.javaweb.entity.User;
import com.epam.javaweb.exception.DaoException;
import com.epam.javaweb.exception.ServiceException;

import java.util.List;
import java.util.Optional;

public class ReviewService {
    private ReviewDao reviewDao;
    private UserService userService;

    public ReviewService(DaoFactory daoFactory, UserService userService) {
        reviewDao = daoFactory.getReviewDao();
        this.userService = userService;
    }

    public List<Review> getReviewsByMovieId(int movieId) throws ServiceException {
        try {
            String movieIdStr = "" + movieId;
            List<Review> reviews = reviewDao.getByMovieId(movieIdStr);
            fetchUserToReviews(reviews);
            return reviews;
        } catch (DaoException e) {
            throw new ServiceException(e);
        }
    }

    public List<Review> getReviewsByUserId(int userId) throws ServiceException {
        try {
            String userIdStr = "" + userId;
            List<Review> reviews = reviewDao.getByUserId(userIdStr);
            fetchUserToReviews(reviews);
            return reviews;
        } catch (DaoException e) {
            throw new ServiceException(e);
        }
    }

    public List<Review> getAllReviews() throws ServiceException {
        try {
            List<Review> reviews = reviewDao.getAll();
            fetchUserToReviews(reviews);
            return reviews;
        } catch (DaoException e) {
            throw new ServiceException(e);
        }
    }

    public void addReview(int movieId, int userId, float rate, String reviewStr) throws ServiceException {
        try {
            Review review = new Review();
            review.setMovieId(movieId);
            review.setUserId(userId);
            review.setRate(rate);
            review.setReview(reviewStr);
            reviewDao.save(review);
        } catch (DaoException e) {
            throw new ServiceException(e);
        }
    }

    public void deleteReviewById(int reviewId) throws ServiceException{
        try {
            reviewDao.removeById((long) reviewId);
        } catch (DaoException e) {
            throw new ServiceException(e);
        }
    }

    private void fetchUserToReviews(List<Review> reviews) throws ServiceException {
        for (Review review : reviews) {
            fetchUserToReview(review);
        }
    }

    private void fetchUserToReview(Review review) throws ServiceException {
        if (review != null) {
            int userId = review.getUserId();
            Optional<User> optionalUser = userService.findById(userId);
            if (optionalUser.isPresent()) {
                User user = optionalUser.get();
                review.setUser(user);
            }
        }
    }
}
