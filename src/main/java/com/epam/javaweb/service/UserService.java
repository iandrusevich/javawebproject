package com.epam.javaweb.service;

import com.epam.javaweb.dao.DaoFactory;
import com.epam.javaweb.dao.UserDao;
import com.epam.javaweb.entity.Review;
import com.epam.javaweb.entity.User;
import com.epam.javaweb.exception.DaoException;
import com.epam.javaweb.exception.ServiceException;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class UserService {
    private UserDao userDao;
    private static final int MINIMAL_STATUS = 1;
    private static final int MAXIMAL_STATUS = 7;
    private final static float MAXIMAL_RATE_DISPERSION = 2;
    private final static int NUMBER_OF_REVIEWS_FOR_STATUS_RECALCULATION = 3;

    public UserService(DaoFactory daoFactory) {
        userDao = daoFactory.getUserDao();
    }

    public Optional<User> login(String login, String password) throws ServiceException {
        try {
            return userDao.findUserByLoginAndPassword(login, password);
        } catch (DaoException e) {
            throw new ServiceException(e);
        }
    }

    public Optional<User> findById(int id) throws ServiceException {
        try {
            return userDao.getById((long) id);
        } catch (DaoException e) {
            throw new ServiceException(e);
        }
    }

    public Optional<User> findBySessionId(String sessionId) throws ServiceException {
        try {
            return userDao.findUserBySessionId(sessionId);
        } catch (DaoException e) {
            throw new ServiceException(e);
        }
    }

    public void addSessionId(int userId, String sessionId) throws ServiceException {
        try {
            userDao.addSessionId((long) userId, sessionId);
        } catch (DaoException e) {
            throw new ServiceException(e);
        }
    }

    public void deleteSessionId(String sessionId) throws ServiceException {
        try {
            userDao.deleteSessionId(sessionId);
        } catch (DaoException e) {
            throw new ServiceException(e);
        }
    }

    public List<User> getAllNonAdminUsers() throws ServiceException {
        try {
            List<User> allUsers = userDao.getAll();
            List<User> allNonAdminUsers = new ArrayList<>();
            for (User user : allUsers) {
                if (!user.isAdmin()) {
                    allNonAdminUsers.add(user);
                }
            }
            return allNonAdminUsers;
        } catch (DaoException e) {
            throw new ServiceException(e);
        }
    }

    public void changeUserActivity(User user) throws ServiceException {
        try {
            long idLong = user.getId();
            if (user.isActive()) {
                userDao.changeUserActivityById(idLong, false);
            } else {
                userDao.changeUserActivityById(idLong, true);
            }
        } catch (DaoException e) {
            throw new ServiceException(e);
        }
    }

    public void changeUserStatus(User user, boolean isIncrease) throws ServiceException {
        int userStatusId = user.getStatusId();
        try {
            if (isStatusChangeAllowed(userStatusId, isIncrease)) {
                long idLong = user.getId();
                userDao.changeUserStatusById(idLong, isIncrease);
            }
        } catch (DaoException e) {
            throw new ServiceException(e);
        }
    }

    public void recalculateReviewersStatuses(float averageRating, List<Review> reviews, User currentUser) throws ServiceException {
        if (reviews.size() == NUMBER_OF_REVIEWS_FOR_STATUS_RECALCULATION) {
            for (Review review : reviews) {
                recalculateReviewerStatus(averageRating, review);
            }
        }
        if (reviews.size() > NUMBER_OF_REVIEWS_FOR_STATUS_RECALCULATION) {
            Review currentUserReview = findCurrentUserReview(reviews, currentUser);
            recalculateReviewerStatus(averageRating, currentUserReview);
        }
    }

    public void recalculateReviewerStatus(float averageRating, Review review) throws ServiceException {
        float rate = review.getRate();
        float dispersion = (Math.abs(averageRating - rate));
        boolean isIncrease = (dispersion <= MAXIMAL_RATE_DISPERSION);
        User user = review.getUser();
        changeUserStatus(user, isIncrease);
    }

    public boolean isStatusChangeAllowed(int userStatusId, boolean isIncrease) {
        return !(isUserStatusTooLow(userStatusId, isIncrease) || isUserStatusTooHigh(userStatusId, isIncrease));
    }

    public boolean isUserStatusTooLow(int userStatusId, boolean isIncrease) {
        return userStatusId <= MINIMAL_STATUS && !isIncrease;
    }

    public boolean isUserStatusTooHigh(int userStatusId, boolean isIncrease) {
        return userStatusId >= MAXIMAL_STATUS && isIncrease;
    }

    private Review findCurrentUserReview(List<Review> reviews, User currentUser) {
        Review currentUserReview = null;
        for (Review review : reviews) {
            if (review.getUserId() == currentUser.getId()) {
                currentUserReview = review;
            }
        }
        return currentUserReview;
    }
}
