package com.epam.javaweb.servlet;

import com.epam.javaweb.command.Command;
import com.epam.javaweb.command.CommandFactory;
import com.epam.javaweb.command.CommandResult;
import com.epam.javaweb.dao.ConnectionPool;
import com.epam.javaweb.dao.DaoFactory;
import com.epam.javaweb.util.Redirector;
import com.epam.javaweb.util.ShowResultMessageHelper;
import org.apache.log4j.Logger;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.Connection;

public class ServletController extends HttpServlet {
    private final static Logger LOGGER = Logger.getLogger(ServletController.class);

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        process(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        process(request, response);
    }

    private void process(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        CommandResult commandResult = null;
        HttpSession session = request.getSession();
        try (Connection connection = ConnectionPool.getInstance().getConnection()) {
            String commandStr = request.getParameter("command");
            DaoFactory daoFactory = new DaoFactory(connection);
            String sessionId = session.getId();
            CommandFactory commandFactory = new CommandFactory(daoFactory, sessionId);
            Command command = commandFactory.createCommand(commandStr);
            commandResult = command.execute(request, response);
            connection.commit();
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            ShowResultMessageHelper.setDisplayedMessage(session, false, null);
            commandResult = Redirector.redirectToPreviousGetRequest(session);
        } finally {
            dispatch(commandResult, request, response);
        }
    }

    private void dispatch(CommandResult commandResult, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String page = commandResult.getPage();
        if (commandResult.isForward()) {
            RequestDispatcher dispatcher = request.getRequestDispatcher(page);
            dispatcher.forward(request, response);
        } else {
            response.sendRedirect(page);
        }
    }
}


