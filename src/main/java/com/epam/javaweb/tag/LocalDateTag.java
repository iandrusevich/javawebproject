package com.epam.javaweb.tag;

import java.text.DateFormat;
import java.util.Date;
import java.util.Locale;

public class LocalDateTag {
    public static String localizeDateFormat(Date date, Locale locale){
        DateFormat dateFormat = DateFormat.getDateInstance(DateFormat.LONG, locale);
        return dateFormat.format(date);
    }
}
