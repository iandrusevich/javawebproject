package com.epam.javaweb.util;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

public class Paginator {
    private HttpServletRequest request;
    private HttpServletResponse response;
    private final int NUMBER_OF_DISPLAYED_PAGES_AT_THE_BEGINNING = 1;
    private final int NUMBER_OF_DISPLAYED_PAGES_BEFORE_CURRENT = 1;
    private final int NUMBER_OF_DISPLAYED_PAGES_AFTER_CURRENT = 1;
    private final int NUMBER_OF_DISPLAYED_PAGES_IN_THE_END = 1;

    public Paginator(HttpServletRequest request, HttpServletResponse response) {
        this.request = request;
        this.response = response;
    }

    public void setPaginationIntoRequest(List items, int currentPage, int pageSize, String url) {
        int totalNumberOfRecords = items.size();
        int totalNumberOfPages = 0;
        if (totalNumberOfRecords % pageSize == 0) {
            totalNumberOfPages = totalNumberOfRecords / pageSize;
        } else {
            totalNumberOfPages = totalNumberOfRecords / pageSize + 1;
        }
        request.setAttribute("url", url);
        request.setAttribute("last_page", totalNumberOfPages);
        request.setAttribute("total_records", totalNumberOfRecords);
        request.setAttribute("current_page", currentPage);
        request.setAttribute("page_size", pageSize);

        boolean showAllPrev = NUMBER_OF_DISPLAYED_PAGES_AT_THE_BEGINNING >= (currentPage - NUMBER_OF_DISPLAYED_PAGES_BEFORE_CURRENT);
        boolean showAllNext = currentPage + NUMBER_OF_DISPLAYED_PAGES_AFTER_CURRENT > totalNumberOfPages - NUMBER_OF_DISPLAYED_PAGES_IN_THE_END;
        request.setAttribute("N_PAGES_FIRST", NUMBER_OF_DISPLAYED_PAGES_AT_THE_BEGINNING);
        request.setAttribute("N_PAGES_PREV", NUMBER_OF_DISPLAYED_PAGES_BEFORE_CURRENT);
        request.setAttribute("N_PAGES_NEXT", NUMBER_OF_DISPLAYED_PAGES_AFTER_CURRENT);
        request.setAttribute("N_PAGES_LAST", NUMBER_OF_DISPLAYED_PAGES_IN_THE_END);
        request.setAttribute("showAllPrev", showAllPrev);
        request.setAttribute("showAllNext", showAllNext);
    }

    public <T> List<T> getItemsForCurrentPage(List<T> items, int currentPage, int pageSize) {
        int firstItemForCurrentPage = (currentPage - 1) * pageSize;
        int lastItemForCurrentPage = firstItemForCurrentPage + pageSize;
        if (lastItemForCurrentPage > items.size() - 1) {
            lastItemForCurrentPage = items.size();
        }
        return items.subList(firstItemForCurrentPage, lastItemForCurrentPage);
    }
}
