package com.epam.javaweb.util;

import com.epam.javaweb.command.CommandResult;

import javax.servlet.http.HttpSession;

public class Redirector {

    public static CommandResult redirectToPreviousGetRequest(HttpSession session) {
        String previousSuccessfulGetRequest = (String) session.getAttribute("previousGetRequest");
        if (previousSuccessfulGetRequest == null) {
            session.setAttribute("previousGetRequest", "?command=showLoginPage");
            return new CommandResult("WEB-INF/login.jsp", true);
        } else {
            return new CommandResult(previousSuccessfulGetRequest, false);
        }
    }
}
