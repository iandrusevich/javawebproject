package com.epam.javaweb.util;

import com.epam.javaweb.entity.Movie;
import com.epam.javaweb.entity.Review;
import com.epam.javaweb.exception.ServiceException;
import com.epam.javaweb.service.MovieService;

import java.util.List;
import java.util.Optional;

public class ReviewValidator {
    private MovieService movieService;
    private final static int REVIEW_MAX_LENGTH = 300;

    public ReviewValidator(MovieService movieService){
        this.movieService = movieService;
    }
    public boolean isReviewValid(int movieId, int userId, float rate, String reviewStr) throws ServiceException {
        if (rate < 0 || rate > 10 || reviewStr.length() > REVIEW_MAX_LENGTH){
            return false;
        }
        Optional<Movie> optionalMovie = movieService.getById(movieId);
        if (!optionalMovie.isPresent()) {
            return false;
        }
        Movie movie = optionalMovie.get();
        List<Review> reviews = movie.getReviews();
        for (Review review : reviews){
            if (review.getUserId() == userId){
                return false;
            }
        }
        return true;
    }
}
