package com.epam.javaweb.util;

import javax.servlet.http.HttpSession;
import java.util.Locale;
import java.util.ResourceBundle;

public class ShowResultMessageHelper {

    public static void setDisplayedMessage(HttpSession session, boolean isResultSuccessful, String messageKey) {
        Locale locale = (Locale) session.getAttribute("locale");
        ResourceBundle messages = ResourceBundle.getBundle("MessagesBundle", locale);
        String message;
        if (messageKey != null) {
            message = messages.getString(messageKey);
        } else if (isResultSuccessful) {
            message = messages.getString("general.general.success-message");
        } else {
            message = messages.getString("general.general.error-message");
        }

        if (isResultSuccessful) {
            session.setAttribute("successMessage", message);
            session.setAttribute("isSuccessMessageDisplayed", false);
        } else {
            session.setAttribute("errorMessage", message);
            session.setAttribute("isErrorMessageDisplayed", false);
        }
    }
}
