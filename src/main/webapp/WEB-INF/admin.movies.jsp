<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="/WEB-INF/tld/custom.tld" prefix="ctg" %>

<!DOCTYPE html>
<html>

<head>
    <link rel="stylesheet" href="css/styles.css">
    <title>Movies</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

</head>

<body>
    <%@include file="includes/messages-panels.jsp" %>
    <nav>
        <div class="left-menu-group">
            <form>
            <button type="submit" name="command" value="showAdminMovies"autofocus>${webPagesContent.getString("header.movies-button-label")}</button>
            <button type="submit" name="command" value="showUsers">${webPagesContent.getString("header.users-button-label")}</button>
            </form>
        </div>
        <div class="right-menu-group">
            <%@include file="includes/language-change-panel.jsp" %>
            <form>
            <button type="submit" name="command" value="logout">${webPagesContent.getString("header.logout-button-label")}</button>
            </form>
        </div>
    </nav>
    <div class="table-wrapper movies-table">
        <table>
            <tbody>
                <tr>
                    <th></th>
                    <th></th>
                    <th>${webPagesContent.getString("movies.table.title-label")}</th>
                    <th>${webPagesContent.getString("movies.table.year-label")}</th>
                    <th>${webPagesContent.getString("movies.table.genre-label")}</th>
                    <th>${webPagesContent.getString("movies.table.rating-label")}</th>
                    <th>${webPagesContent.getString("movies.table.producer-label")}</th>
                    <th></th>
                </tr>
            </tbody>

            <c:forEach var="movie" items="${movies}">

            <%@include file="includes/movies-accordion.jsp" %>

            <tbody class="panel">
                <c:forEach var="review" items="${movie.reviews}">
                    <tr>
                        <td></td>
                        <td></td>
                        <td>${review.user.name} ${review.user.surname}</td>
                        <td><span class="star">&#9733;</span> ${review.rate}</td>
                        <td class="description" colspan="3">${review.review}</td>
                        <td>
                            <a href="?command=deleteReview&reviewId=${review.id}&movieId=${movie.id}">
                                <img src="img/trash.png" class="trash" />
                            </a>
                        </td>
                    </tr>
                </c:forEach>
            </tbody>

            </c:forEach>

            <form action="?command=addMovie" method="post" enctype='multipart/form-data'>
            <tbody>
                <tr>
                    <td></td>
                    <td><input name="image" type="file" accept=".jpg, .jpeg, .png" required oninvalid="InvalidMsg(this);" oninput="InvalidMsg(this);"</td>
                    <td>
                        <input name="title" placeholder="${webPagesContent.getString("movies.table.new-movie.title.placeholder")}" required maxlength="250" oninvalid="InvalidMsg(this);" oninput="InvalidMsg(this);"></input>
                    </td>
                    <td>
                        <input name="premiere" placeholder="${webPagesContent.getString("movies.table.new-movie.title.placeholder")}" required type="date" min="1880-01-01" oninvalid="InvalidMsg(this);" oninput="InvalidMsg(this);"></input>
                    </td>
                    <td>
                        <select name="genreId" placeholder="${webPagesContent.getString("movies.table.new-movie.genre.placeholder")}" required oninvalid="InvalidMsg(this);" oninput="InvalidMsg(this);">
                            <c:forEach var="genre" items="${genres}">
                                <option value="${genre.id}">${genre.name}</option>
                            </c:forEach>
                        </select>
                    </td>
                    <td><span class="star">&#9733;</span> 0.0</td>
                    <td>
                        <input name="producer" placeholder="${webPagesContent.getString("movies.table.new-movie.producer.placeholder")}" required maxlength="300" oninvalid="InvalidMsg(this);" oninput="InvalidMsg(this);"></input>
                    </td>
                    <td>
                        <input type="submit" class="body-button" value="${webPagesContent.getString("movies.table.new-movie.add-button-label")}"></input>
                    </td>
                </tr>
            </tbody>
            </form>
         </table>
    </div>

<%@include file="includes/pagination-script.jsp" %>

</body>

<footer>
    <p>${webPagesContent.getString("footer.author-label")}</p>
</footer>

<script src="js/accordion.js"></script>
<script src="js/error-message.js"></script>
<script src="js/success-message.js"></script>
<%@include file="includes/validation-script.jsp" %>

</html>