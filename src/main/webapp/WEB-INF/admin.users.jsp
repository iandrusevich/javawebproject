<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="/WEB-INF/tld/custom.tld" prefix="ctg" %>

<!DOCTYPE html>
<html>

<head>
    <link rel="stylesheet" href="css/styles.css">
    <title>Users</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

</head>

<body>
    <%@include file="includes/messages-panels.jsp" %>
    <nav>
        <div class="left-menu-group">
            <form>
            <button type="submit" name="command" value="showAdminMovies">${webPagesContent.getString("header.movies-button-label")}</button>
            <button type="submit" autofocus>${webPagesContent.getString("header.users-button-label")}</button>
            </form>
        </div>
		<div class="right-menu-group">
            <%@include file="includes/language-change-panel.jsp" %>
            <form>
            <button type="submit" name="command" value="logout">${webPagesContent.getString("header.logout-button-label")}</button>
            </form>
		</div>
    </nav>

	<div class="table-wrapper user-table">
        <table>
            <tr>
				<th>${webPagesContent.getString("users.table.first-name-label")}</th>
                <th>${webPagesContent.getString("users.table.last-name-label")}</th>
                <th>${webPagesContent.getString("users.table.username-label")}</th>
                <th>${webPagesContent.getString("users.table.active-label")}</th>
				<th></th>
                <th>${webPagesContent.getString("users.table.status-label")}</th>
				<th></th>
            </tr>
            <c:forEach var="nonAdminUser" items="${nonAdminUsers}">
            <tr>
                <td><c:out value="${nonAdminUser.name}"/></td>
                <td><c:out value="${nonAdminUser.surname}"/></td>
                <td><c:out value="${nonAdminUser.login}"/></td>
                <td>
                    <form>
                        <input type="hidden" name="command" value="changeUserActivity"></input>
                        <input type="hidden" name="userId" value="${nonAdminUser.id}"></input>
                        <div class="checkbox-container">
                            <input type="checkbox"
                                <c:if test="${nonAdminUser.active}">
                                    checked
                                </c:if>
                                onChange="this.form.submit()">
                            </input>
                        </div>
                    </form>
                </td>
                <form>
                <input type="hidden" name="command" value="changeUserStatus"/>
                <input type="hidden" name="userId" value="${nonAdminUser.id}"></input>
				<td><button class="body-button right-align-button" type="submit" name="isIncrease" value="false">${webPagesContent.getString("users.table.down-button-label")}</button></td>
                <td class="button-row"><div class="status", status="<c:out value="${nonAdminUser.statusId}"/>"><c:out value="${nonAdminUser.status}"/></div></td>
				<td><button class="body-button left-align-button" type="submit" name="isIncrease" value="true">${webPagesContent.getString("users.table.up-button-label")}</button></td>
				</form>
            </tr>
            </c:forEach>
		</table>
		</div>

<%@include file="includes/pagination-script.jsp" %>

</body>
<footer>
    <p>By Ivan Andrusevich</p>
</footer>

<script src="js/error-message.js"></script>
<script src="js/success-message.js"></script>
<%@include file="includes/validation-script.jsp" %>

</html>