            <ul>
                <li><a href="${req.getContextPath}?command=locale&language=en&country=US">${webPagesContent.getString("header.en-language-button")}</a></li>
                <li><a href="${req.getContextPath}?command=locale&language=ru&country=RU">${webPagesContent.getString("header.ru-language-button")}</a></li>
                <li><a href="${req.getContextPath}?command=locale&language=be&country=BY">${webPagesContent.getString("header.by-language-button")}</a></li>
            </ul>