<script>
            function InvalidMsg(textbox) {
            var valueLength = textbox.value.length;
            var maxlength = textbox.getAttribute('maxlength');
            var minlength = textbox.getAttribute('minlength');
            var max = textbox.getAttribute('max');
            var min = textbox.getAttribute('min');
            var step = textbox.getAttribute('step');

                if (textbox.validity.patternMismatch) {
                    textbox.setCustomValidity
                          ('${messages.getString("validation.patternMismatch.message")}');
                } else if (textbox.validity.valueMissing) {
                    textbox.setCustomValidity
                          ('${messages.getString("validation.valueMissing.message")}');
                } else if ((maxlength != null) && (maxlength < valueLength)) {
                    textbox.setCustomValidity
                          ('${messages.getString("validation.tooLong.first.message")}' + maxlength + ' ${messages.getString("validation.tooLong.second.message")}' + valueLength);
                } else if ((minlength != null) && (minlength > valueLength)) {
                    textbox.setCustomValidity
                          ('${messages.getString("validation.tooShort.first.message")}' + minlength + ' ${messages.getString("validation.tooShort.second.message")}' + valueLength);
                } else if (textbox.validity.rangeOverflow) {
                    textbox.setCustomValidity
                          ('${messages.getString("validation.rangeOverflow.message")}' + max +'!');
                } else if (textbox.validity.rangeUnderflow) {
                    textbox.setCustomValidity
                          ('${messages.getString("validation.rangeUnderflow.message")}' + min +'!');
                } else if (textbox.validity.stepMismatch) {
                    textbox.setCustomValidity
                          ('${messages.getString("validation.stepMismatch.message")}' + step +'!');
                } else {
                    textbox.setCustomValidity('');
                }

                return true;
            }
    </script>