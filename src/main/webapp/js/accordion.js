    var acc = document.getElementsByClassName("accordion");
    var i;

    for (i = 0; i < acc.length; i++) {
        var panel = acc[i].nextElementSibling;
        panel.style.display = "none"
        acc[i].addEventListener("click", function() {
            var panel = this.nextElementSibling;
            this.classList.toggle("active");
            if (panel.style.display === "none") {
                panel.style.display = null;
            } else {
                panel.style.display = "none";
            }
        });
    }