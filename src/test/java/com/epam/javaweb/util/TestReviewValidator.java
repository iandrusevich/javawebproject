package com.epam.javaweb.util;

import com.epam.javaweb.entity.Movie;
import com.epam.javaweb.entity.Review;
import com.epam.javaweb.exception.ServiceException;
import com.epam.javaweb.service.MovieService;
import com.tngtech.java.junit.dataprovider.DataProvider;
import com.tngtech.java.junit.dataprovider.DataProviderRunner;
import com.tngtech.java.junit.dataprovider.UseDataProvider;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.testng.Assert;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@RunWith(DataProviderRunner.class)
public class TestReviewValidator {
    private static final MovieService movieService = Mockito.mock(MovieService.class);
    private static final ReviewValidator validator = new ReviewValidator(movieService);
    private static final int MOVIE_ID = 1;

    @DataProvider
    public static Object[][] reviewDataForPositiveScenario() {
        return new Object[][]{
                {0f, createReviewStr(300), 2, 1},
                {5.0f, createReviewStr(300), 2, 1},
                {10.f, createReviewStr(300), 2, 1}
        };
    }

    @DataProvider
    public static Object[][] reviewDataForNegativeScenario() {
        return new Object[][]{
                {-0.99f, createReviewStr(300), 2, 1},
                {10.01f, createReviewStr(300), 2 ,1},
                {0.f, createReviewStr(301), 2, 1},
                {0.f, createReviewStr(300), 1, 1},
        };
    }

    @Test
    @UseDataProvider("reviewDataForPositiveScenario")
    public void testIsReviewValidPositive(float rate, String reviewStr, int userIdToCheck, int userIdInReview) throws ServiceException {
        // given
        Mockito.when(movieService.getById(Mockito.anyInt())).thenReturn(createMovie(userIdInReview));

        // when
        boolean actual = validator.isReviewValid(MOVIE_ID, userIdToCheck, rate, reviewStr);

        // then
        Assert.assertTrue(actual);
    }

    @Test
    @UseDataProvider("reviewDataForNegativeScenario")
    public void testIsReviewValidNegative(float rate, String reviewStr, int userIdToCheck, int userIdInReview) throws ServiceException {
        // given
        Mockito.when(movieService.getById(Mockito.anyInt())).thenReturn(createMovie(userIdInReview));

        //when
        boolean actual = validator.isReviewValid(MOVIE_ID, userIdToCheck, rate, reviewStr);

        //then
        Assert.assertFalse(actual);
    }

    private static Optional<Movie> createMovie(int userId) {
        Movie movie = new Movie();
        Review review = new Review();
        review.setUserId(userId);
        List<Review> reviews = new ArrayList<>();
        reviews.add(review);
        movie.setReviews(reviews);
        return Optional.of(movie);
    }

    private static String createReviewStr(int length) {
        StringBuilder stringBuilder = new StringBuilder();
        for (int i = 0; i < length; i++) {
            stringBuilder.append("A");
        }
        return stringBuilder.toString();
    }
}
