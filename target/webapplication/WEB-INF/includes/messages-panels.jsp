<div class="error-message">
    <div class="message-text-container">${errorMessage}</div>
    <div class="message-button-container">
        <button class="error-message-button">x</button>
    </div>
</div>
<div class="success-message">
    <div class="message-text-container">${successMessage}</div>
    <div class="message-button-container">
        <button class="success-message-button">x</button>
    </div>
</div>