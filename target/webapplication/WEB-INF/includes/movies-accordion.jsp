<tbody class="accordion">
    <tr>
    <td class="accordion-arrow"></td>
    <td><img class="poster" src="img/posters/${movie.image}"></td>
    <td>${movie.title}</td>
    <td>${ctg:localdate(movie.premiere, locale)}</td>
    <td>${movie.genre}</td>
    <td><span class="star">&#9733;</span> ${movie.averageRating}</td>
    <td>${movie.producer}</td>
    <td></td>
</tr>
</tbody>