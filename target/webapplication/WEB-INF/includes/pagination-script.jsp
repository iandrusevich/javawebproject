<div class="pagination">
    <a href="${url}&page=${current_page-1>=1?current_page-1:1}" class="prev">&lt; ${webPagesContent.getString("movies.table.pagination.prev-button-label")}</a>
    <%-- show left pages --%>
    <c:choose>
        <c:when test="${showAllPrev}">
            <c:if test="${current_page > 1}">
                <c:forEach begin="1" end="${current_page - 1}" var="p">
                    <a href="${url}&page=${p}">${p}</a>
                </c:forEach>
            </c:if>
        </c:when>
        <c:otherwise>
            <c:forEach begin="1" end="${N_PAGES_FIRST}" var="p">
                <a href="${url}&page=${p}">${p}</a>
            </c:forEach>
            <span style="margin-right: 5px">...</span>
            <c:forEach begin="${current_page - N_PAGES_PREV}" end="${current_page - 1}" var="p">
                <a href="${url}&page=${p}">${p}</a>
            </c:forEach>
        </c:otherwise>
    </c:choose>
    <%-- show current page --%>
    <a href="${url}&page=${current_page}" class="current">${current_page}</a>
    <%-- show right pages --%>
    <c:choose>
        <c:when test="${showAllNext}">
            <c:forEach begin="${current_page + 1}" end="${last_page}" var="p">
                <a href="${url}&page=${p}">${p}</a>
            </c:forEach>
        </c:when>
        <c:otherwise>
            <c:forEach begin="${current_page + 1}" end="${current_page + 1 + (N_PAGES_NEXT - 1)}" var="p">
                <a href="${url}&page=${p}">${p}</a>
            </c:forEach>
            <span style="margin-right: 5px">...</span>
            <c:forEach begin="${last_page - (N_PAGES_LAST - 1)}" end="${last_page}" var="p">
                <a href="${url}&page=${p}">${p}</a>
            </c:forEach>
        </c:otherwise>
    </c:choose>
    <a href="${url}&page=${current_page + 1 > last_page ? last_page : current_page + 1}" class="next">${webPagesContent.getString("movies.table.pagination.next-button-label")} &gt;</a>
</div>