<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html>

<head>
    <link rel="stylesheet" href="css/styles.css">
    <title>Movies</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

</head>

<body>
    <%@include file="includes/messages-panels.jsp" %>
    <nav>
        <div class="right-menu-group">
            <%@include file="includes/language-change-panel.jsp" %>
        </div>
    </nav>
    <div class="login-block">
        <form action="?command=login" method="post">
            <input type="text" name="login" placeholder="${webPagesContent.getString("login.login-placeholder")}" required minlength="4" maxlength="50" oninvalid="InvalidMsg(this);" oninput="InvalidMsg(this);"></input>
            <input type="password" name = "password" placeholder="${webPagesContent.getString("login.password-placeholder")}" required minlength="3" maxlength="50" oninvalid="InvalidMsg(this);" oninput="InvalidMsg(this);"></input>
            <input type="submit" value="${webPagesContent.getString("login.login-button-label")}" class="body-button more-button"></input>
        </form>
    </div>
</body>
<footer>
    <p>${webPagesContent.getString("footer.author-label")}</p>
</footer>

<script src="js/error-message.js"></script>
<script src="js/success-message.js"></script>
<%@include file="includes/validation-script.jsp" %>

</html>