<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="/WEB-INF/tld/custom.tld" prefix="ctg" %>

<!DOCTYPE html>
<html>

<head>
    <link rel="stylesheet" href="css/styles.css">

    <title>Movies</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

</head>

<body>
    <%@include file="includes/messages-panels.jsp" %>
    <nav>
        <div class="left-menu-group">
            <form>
            <button type="submit" name="command" value="showUserMovies"autofocus>${webPagesContent.getString("header.movies-button-label")}</button>
            </form>
        </div>
        <div class="right-menu-group">
            <div class="status" status="${user.statusId}">${user.status}</div>
            <%@include file="includes/language-change-panel.jsp" %>
            <form>
            <button type="submit" name="command" value="logout">${webPagesContent.getString("header.logout-button-label")}</button>
            </form>
        </div>
    </nav>
    <div class="table-wrapper movies-table">
        <table>
            <tbody>
                <tr>
                    <th></th>
                    <th></th>
                    <th>${webPagesContent.getString("movies.table.title-label")}</th>
                    <th>${webPagesContent.getString("movies.table.year-label")}</th>
                    <th>${webPagesContent.getString("movies.table.genre-label")}</th>
                    <th>${webPagesContent.getString("movies.table.rating-label")}</th>
                    <th>${webPagesContent.getString("movies.table.producer-label")}</th>
                    <th></th>
                </tr>
            </tbody>

            <c:forEach var="movie" items="${movies}">
                <%@include file="includes/movies-accordion.jsp" %>
                <tbody class="panel">
                <c:set var="displayReviewAdd" value="${true}"/>
                    <c:forEach var="review" items="${movie.reviews}">
                    <c:if test="${review.userId == user.id}"> <c:set var="displayReviewAdd" value="${false}" /></c:if>
                         <tr>
                             <td></td>
                             <td></td>
                             <td>${review.user.name} ${review.user.surname}</td>
                             <td><span class="star">&#9733;</span> ${review.rate}</td>
                             <td class="description" colspan="5">${review.review}</td>
                         </tr>

                    </c:forEach>
                        <c:if test="${displayReviewAdd}">
                            <tr>
                                <form action="?command=addRate" method="post">
                                <input type="hidden" name="movieId" value="${movie.id}"></input>
                                <td></td>
                                <td></td>
                                <td><b>${webPagesContent.getString("movies.table.you-label")}</b></td>
                                <td class="rate-column"><span class="star"></span>
                                    <input name="rate" type="number" step="0.1" min="0" max="10" placeholder="${webPagesContent.getString("movies.table.your-rate.placeholder")}" required oninvalid="InvalidMsg(this);" oninput="InvalidMsg(this);"></input>
                                </td>
                                <td class="description" colspan="3">
                                    <input name="review" type="text-area" placeholder="${webPagesContent.getString("movies.table.your-review.placeholder")}" maxlength="1000" oninvalid="InvalidMsg(this);" oninput="InvalidMsg(this);"></input>
                                </td>
                                <td>
                                    <button class="body-button">${webPagesContent.getString("movies.table.new-review.add-button-label")}</button>
                                </td>
                                </form>
                            </tr>
                        </c:if>
            </tbody>
            </c:forEach>
        </table>
    </div>
<%@include file="includes/pagination-script.jsp" %>


</body>
<footer>
    <p>${webPagesContent.getString("footer.author-label")}</p>
</footer>

<script src="js/accordion.js"></script>
<script src="js/error-message.js"></script>
<script src="js/success-message.js"></script>
<%@include file="includes/validation-script.jsp" %>
</html>