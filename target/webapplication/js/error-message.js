    var acc = document.getElementsByClassName("error-message");
    var i;

    for (i = 0; i < acc.length; i++) {
        acc[i].addEventListener("click", function() {
            this.style.display = "none";
        });
        var messageContainer = acc[i].firstElementChild;
        if (messageContainer.textContent == "") {
            acc[i].style.display = "none";
        }
    }

